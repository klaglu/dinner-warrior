package io.swagger.api;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import io.swagger.model.Game;
import io.swagger.model.User;
import io.swagger.repository.GameRepository;
import io.swagger.repository.UserRepository;
import java.util.Random;

@CrossOrigin()
@Controller
public class GameController {

	@Autowired
	GameRepository gameRepository;
	
	@Autowired
	UserRepository userRepository;

	@RequestMapping(value = "/game", produces = { "application/json" }, method = RequestMethod.POST)
	public ResponseEntity<Game> createGame(@RequestBody Game body) {
		Game game = null;
		try {

			Date data = new Date();
			Random rand = new Random();
			int  n = rand.nextInt(500) + 1;
			body.setRandNumber(n);
			body.setHash(String.valueOf(data.getTime()));
			game = gameRepository.save(body);
		} catch (Exception e) {
			return new ResponseEntity<Game>(HttpStatus.I_AM_A_TEAPOT);
		}

		return new ResponseEntity<Game>(game, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/game/{hash}", produces = { "application/json" }, method = RequestMethod.GET)
	public ResponseEntity<List<Game>> getGameByHash(@PathVariable String hash) {
		List<Game> game = null;
		try {
			game = gameRepository.findByHash(hash);
		} catch (Exception e) {
			return new ResponseEntity<List<Game>>(HttpStatus.I_AM_A_TEAPOT);
		}

		return new ResponseEntity<List<Game>>(game, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/game/users/{hash}", produces = { "application/json" }, method = RequestMethod.GET)
	public ResponseEntity<List<User>> getUsersByHashGame(@PathVariable String hash) {
		List<User> users = null;
		try {
			List<Game> game = gameRepository.findByHash(hash);
			users = userRepository.findByGameId(game.get(0).getId());
		} catch (Exception e) {
			return new ResponseEntity<List<User>>(HttpStatus.I_AM_A_TEAPOT);
		}

		return new ResponseEntity<List<User>>(users, HttpStatus.CREATED);
	}
}
