package io.swagger.api;

import io.swagger.model.Game;
import io.swagger.model.User;
import io.swagger.repository.GameRepository;
import io.swagger.repository.UserRepository;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-04-17T14:07:18.385Z")

@CrossOrigin()
@Controller
public class UserApiController implements UserApi {

	@Autowired
	UserRepository userRepository;

	@Autowired
	GameRepository gameRepository;

	private static final Logger log = LoggerFactory.getLogger(UserApiController.class);

	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public UserApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	public ResponseEntity<User> createUser(
			@ApiParam(value = "Created user object", required = true) @Valid @RequestBody User body,
			@RequestParam("hash") String hash) {
		User user = null;
		try {
			body.setScore(0);
			List<Game> game = gameRepository.findByHash(hash);
			body.setGameId(game.get(0).getId());
			user = userRepository.save(body);
		} catch (Exception e) {
			return new ResponseEntity<User>(user, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<User>(user, HttpStatus.CREATED);
	}

	public ResponseEntity<User> getUserByHashName(
			@ApiParam(value = "The name that needs to be fetched. Use user1 for testing. ", required = true) @PathVariable("game_hash") String id) {
		List<User> user = null;
		try {
			user = userRepository.findById(Long.parseLong(id, 10));
		}catch(Exception e) {
			return new ResponseEntity<User>(user.get(0), HttpStatus.I_AM_A_TEAPOT);
		}
				return new ResponseEntity<User>(user.get(0), HttpStatus.ACCEPTED);
	}

	public ResponseEntity<List<User>> updateUser(
			@ApiParam(value = "Update user object", required = true) @Valid @RequestBody User body, @RequestParam("arrow") Integer arrow) {
		List<User> user = null;
		try {
			user = userRepository.findById(body.getId());
			List<Game> game = gameRepository.findById(body.getGameId());
			
			user.get(0).setScore(Math.abs(body.getScore()-game.get(0).getRandNumber())-arrow);
			userRepository.save(user);
		} catch (Exception e) {
			return new ResponseEntity<List<User>>(user, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<List<User>>(user, HttpStatus.OK);
	}

	
	

}
