package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * User
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-04-17T14:07:18.385Z")

@Entity(name = "UserGame")
public class User {
	@Id
	@GeneratedValue
	private Long id = null;

	private Long gameId = null;

	private String username = null;

	private Integer score = null;

	public User id(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * Get id
	 * 
	 * @return id
	 **/
	@ApiModelProperty(value = "")

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User gameId(Long gameId) {
		this.gameId = gameId;
		return this;
	}

	/**
	 * Get gameId
	 * 
	 * @return gameId
	 **/
	@ApiModelProperty(value = "")

	public Long getGameId() {
		return gameId;
	}

	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}

	public User username(String username) {
		this.username = username;
		return this;
	}

	/**
	 * Get username
	 * 
	 * @return username
	 **/
	@ApiModelProperty(required = true, value = "")
	@NotNull

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public User score(Integer score) {
		this.score = score;
		return this;
	}

	/**
	 * Get score
	 * 
	 * @return score
	 **/
	@ApiModelProperty(value = "")

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		User user = (User) o;
		return Objects.equals(this.id, user.id) && Objects.equals(this.gameId, user.gameId)
				&& Objects.equals(this.username, user.username) && Objects.equals(this.score, user.score);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, gameId, username, score);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class User {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    gameId: ").append(toIndentedString(gameId)).append("\n");
		sb.append("    username: ").append(toIndentedString(username)).append("\n");
		sb.append("    score: ").append(toIndentedString(score)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
