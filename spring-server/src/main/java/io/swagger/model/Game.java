package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Game
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-04-17T14:07:18.385Z")
@Entity(name = "Game")
public class Game {
	@Id
	@GeneratedValue
	private Long id = null;

	private String name = null;

	private String dinner = null;

	private Long endDate = null;

	private String hash = null;
	
	private Integer randNumber;

	public Game id(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * Get id
	 * 
	 * @return id
	 **/
	@ApiModelProperty(value = "")

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Game name(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Get name
	 * 
	 * @return name
	 **/
	@ApiModelProperty(example = "Walka o obiad", required = true, value = "")
	@NotNull

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Game dinner(String dinner) {
		this.dinner = dinner;
		return this;
	}

	/**
	 * Get dinner
	 * 
	 * @return dinner
	 **/
	@ApiModelProperty(example = "Pizza hawajska", required = true, value = "")
	@NotNull

	public String getDinner() {
		return dinner;
	}

	public void setDinner(String dinner) {
		this.dinner = dinner;
	}

	public Game endDate(Long endDate) {
		this.endDate = endDate;
		return this;
	}

	/**
	 * Get endDate
	 * 
	 * @return endDate
	 **/
	@ApiModelProperty(required = true, value = "")
	@NotNull

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	public Game hash(String hash) {
		this.hash = hash;
		return this;
	}

	/**
	 * Get hash
	 * 
	 * @return hash
	 **/
	@ApiModelProperty(value = "")

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Game game = (Game) o;
		return Objects.equals(this.id, game.id) && Objects.equals(this.name, game.name)
				&& Objects.equals(this.dinner, game.dinner) && Objects.equals(this.endDate, game.endDate)
				&& Objects.equals(this.hash, game.hash);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, dinner, endDate, hash);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Game {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    dinner: ").append(toIndentedString(dinner)).append("\n");
		sb.append("    endDate: ").append(toIndentedString(endDate)).append("\n");
		sb.append("    hash: ").append(toIndentedString(hash)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	public Integer getRandNumber() {
		return randNumber;
	}

	public void setRandNumber(Integer randNumber) {
		this.randNumber = randNumber;
	}
}
