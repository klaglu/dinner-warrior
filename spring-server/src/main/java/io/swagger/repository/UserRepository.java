package io.swagger.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.swagger.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
	@SuppressWarnings("unchecked")
	User save(User serieEntity);
	List<User> findByGameId(Long id);
	List<User> findById(Long id);
}
