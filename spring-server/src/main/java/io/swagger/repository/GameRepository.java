package io.swagger.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import io.swagger.model.Game;

public interface GameRepository extends CrudRepository<Game, Integer> {
	@SuppressWarnings("unchecked")
	Game save(Game game);
	List<Game> findByHash(String hash);
	List<Game> findById(Long id);
	
}
