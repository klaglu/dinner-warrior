import { DinnerFighterPage } from './app.po';

describe('dinner-fighter App', () => {
  let page: DinnerFighterPage;

  beforeEach(() => {
    page = new DinnerFighterPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
