import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';

@Injectable()
export class UserService {
  Base_url = 'http://localhost:8081/v2/user';

  constructor(private http: HttpClient) { }

  createUserWithHash(user: User, hash: string) {
    return this.http.put(`${this.Base_url}?hash=${hash}`, user);
  }

  updateUserWithHash(user: User, hash: number) {
    user.score = Number(user.score);

    return this.http.post(`${this.Base_url}?arrow=${hash}`, user);
  }

  getUsersByGameHash(hash: string) {
    return this.http.get(`${this.Base_url}/${hash}`);
  }

}
