import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Game } from '../../model/game';

@Injectable()
export class GameService {
  Base_url = 'http://localhost:8081/v2/game';
  constructor(private http: HttpClient) { }

  createGame(game: Game) {
    return this.http.post(this.Base_url, game);
  }

  getGameByHash(hash: string) {
    return this.http.get(`${this.Base_url}/${hash}`);
  }

  getUsersByHash(hash: string) {
    return this.http.get(`${this.Base_url}/users/${hash}`);
  }
}
