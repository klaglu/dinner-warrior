import { Component, OnInit } from '@angular/core';
import { Game } from '../../../model/game';
import { GameService } from '../../services/game.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.css']
})
export class CreateGameComponent implements OnInit {

  name = '';
  desc = '';
  date: number;
  alert = '';
  constructor(private GameServices: GameService, private router: Router) { }

  ngOnInit() {
  }

  dupa() {
    if (this.date !== null) {
      this.alert = '';
      const game: Game = {
        name: this.name,
        dinner: this.desc,
        endDate: 123456
      };
      this.GameServices.createGame(game).subscribe((_game: Game) => {
        this.router.navigate(['/login', _game.hash]);
      },
        () => {
          console.log('error');
        });
    } else {
      this.alert = 'Cyfry!!';
    }
  }

  validate() {
    return this.name === '' || this.desc === '' || this.date == null;
  }
}
