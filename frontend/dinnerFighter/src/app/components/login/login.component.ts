import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { User } from '../../../model/user';
import { ActivatedRoute } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';


class UserClass implements User {
  id?: number;
  gameId?: number;
  username: string;
  score?: number;
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [Location, { provide: LocationStrategy, useClass: PathLocationStrategy }],
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {
  location;

  status = true;
  username = '';
  id: string;
  constructor(private userService: UserService, private route: ActivatedRoute, location: Location,
    private router: Router) { this.location = location; }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('hash');
    this.location = window.location.href;
  }

  createUser() {
    const user = new UserClass();
    user.username = this.username;
    console.log(this.id);
    this.userService.createUserWithHash(user, this.id).subscribe((_series: User) => {
      this.status = false;
      this.router.navigate(['/game', `${this.id}&${_series.id}`]);
    },
      () => {
        console.log('error');
      });
  }

  copy() {

  }

}
