import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  HostListener
} from "@angular/core";
import { UserService } from "../../services/user.service";
import { User } from "../../../model/models";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";

@Component({
  selector: "app-game",
  templateUrl: "./game.component.html",
  styleUrls: ["./game.component.css"]
})
export class GameComponent implements OnInit {
  @ViewChild("myCanvas") canvasRef: ElementRef;
  name = "";
  x = 700;
  arrows_images = [];
  down_arrow = null;
  up_arrow = null;
  left_arrow = null;
  right_arrow = null;
  score = 0;
  arrows = [];
  id = "";
  userID = "";
  hashGame = "";
  user: User;
  nextGame = false;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get("hash");
    const arrayPath = this.id.split("&");
    this.userID = arrayPath[1];
    this.hashGame = arrayPath[0];
    this.getUser();

    this.down_arrow = new Image();
    this.down_arrow.src = "/assets/down.png";
    this.arrows_images.push(this.down_arrow);

    this.up_arrow = new Image();
    this.up_arrow.src = "/assets/up.png";
    this.arrows_images.push(this.up_arrow);

    this.left_arrow = new Image();
    this.left_arrow.src = "/assets/left.png";
    this.arrows_images.push(this.left_arrow);

    this.right_arrow = new Image();
    this.right_arrow.src = "/assets/right.png";
    this.arrows_images.push(this.right_arrow);

    for (let i = 0; i < 20; i++) {
      this.arrows.push({ id: Math.floor(Math.random() * 4), checked: false });
    }

    this.paintLoop()
  }

  paintLoop() {
    let ctx: CanvasRenderingContext2D = this.canvasRef.nativeElement.getContext(
      "2d"
    );
    ctx.clearRect(0, 0, 500, 150);
    this.arrows.forEach((val, index) => {
      ctx.drawImage(this.arrows_images[val.id], this.x + index * 150, 10);
    });
    ctx.rect(186, 10, 128, 128);
    ctx.lineWidth=6;
    ctx.strokeStyle="red";
    ctx.stroke();
    this.x -= 8;
    if (this.x < -3000) {
      this.nextGame = true;
    } else {
      requestAnimationFrame(this.paintLoop.bind(this));
    }
  }

  @HostListener("window:keydown", ["$event"])
  onClick($event) {
    let index = Math.floor(Math.abs((this.x - 235 - 22) / 150));
    if ($event.keyCode == 37)
      this.checkArrow(index, 2);
    if ($event.keyCode == 38)
      this.checkArrow(index, 1);
    if ($event.keyCode == 39)
      this.checkArrow(index, 3);
    if ($event.keyCode == 40)
      this.checkArrow(index, 0);
  }

  checkArrow(index, key) {
    if (index >= 0 && index < 20) {
      if (!this.arrows[index].checked && this.arrows[index].id === key) {
        this.score += 10;
      }
      this.arrows[index].checked = true;
    }
  }

  update() {
    this.user.score = Number(this.name);

    this.userService.updateUserWithHash(this.user, this.score).subscribe(
      (_series: User) => {
        this.router.navigate(["/hall-of-flame", this.hashGame]);
      },
      _ => {
        console.log(_);
      }
    );
  }

  getUser() {
    this.userService.getUsersByGameHash(this.userID).subscribe(
      (_series: User) => {
        this.user = _series;
      },
      () => {
        console.log("error");
      }
    );
  }
}
