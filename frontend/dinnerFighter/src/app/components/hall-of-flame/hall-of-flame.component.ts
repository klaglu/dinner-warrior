import { Component, OnInit } from '@angular/core';
import { GameService } from '../../services/game.service';
import { User } from '../../../model/user';
import { ActivatedRoute } from '@angular/router';
import { Game } from '../../../model/models';

class UserClass implements User {
  id?: number;
  gameId?: number;
  username: string;
  score?: number;
}

@Component({
  selector: 'app-hall-of-flame',
  templateUrl: './hall-of-flame.component.html',
  styleUrls: ['./hall-of-flame.component.css']
})
export class HallOfFlameComponent implements OnInit {

  players = [];
  looser = {};
  hash: string;
  game;
  constructor(private gameService: GameService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.hash = this.route.snapshot.paramMap.get('hash');
    this.getTableUsers();
    this.getGame();
  }

  getTableUsers() {
    this.gameService.getUsersByHash(this.hash).subscribe((users: User[]) => {
      this.players = users.sort(function (a, b) {
        return b.score - a.score;
      });
      this.looser = this.players[0];
    },
      () => {
        console.log('error');
      });
  }

  getGame() {
    this.gameService.getGameByHash(this.hash).subscribe((game: Game[]) => {
      this.game = game[0];
    })
  }

}
