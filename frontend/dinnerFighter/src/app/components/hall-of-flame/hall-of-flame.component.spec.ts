import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HallOfFlameComponent } from './hall-of-flame.component';

describe('HallOfFlameComponent', () => {
  let component: HallOfFlameComponent;
  let fixture: ComponentFixture<HallOfFlameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HallOfFlameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HallOfFlameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
