import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { GameService } from './services/game.service';
import { UserService } from './services/user.service';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { GameComponent } from './components/game/game.component';
import { HallOfFlameComponent } from './components/hall-of-flame/hall-of-flame.component';
import { RouterModule } from '@angular/router';
import {FormsModule} from '@angular/forms';
import { routes } from './app-routing.module';
import { CreateGameComponent } from './components/create-game/create-game.component';

import { MzButtonModule, MzInputModule } from 'ng2-materialize';
import { ClipboardModule } from 'ngx-clipboard';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    GameComponent,
    HallOfFlameComponent,
    CreateGameComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes, { enableTracing:  true, useHash:  true }),
    MzButtonModule,
    MzInputModule,
    ClipboardModule
  ],
  providers: [GameService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
