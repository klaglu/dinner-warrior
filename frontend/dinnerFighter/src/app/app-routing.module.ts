import { GameComponent } from './components/game/game.component';
import { HallOfFlameComponent } from './components/hall-of-flame/hall-of-flame.component';
import { LoginComponent } from './components/login/login.component';
import { CreateGameComponent } from './components/create-game/create-game.component';
import {Routes} from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    component: CreateGameComponent,
    pathMatch: 'full'
  },
  {
    path: 'hall-of-flame/:hash',
    component: HallOfFlameComponent
  },
  {
    path: 'login/:hash',
    component: LoginComponent
  },
  {
    path: 'game/:hash',
    component: GameComponent
  },
  {
    path: '**', redirectTo: ''
  }
];
